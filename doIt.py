#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
import csv
import yaml
import pprint
from collections import defaultdict

parser = argparse.ArgumentParser(description='Analyze a Comdirect Depot')
parser.add_argument('file', nargs='+', type=argparse.FileType('r', encoding='ISO-8859-15'))
parser.add_argument('--debug', action='store_const', const=True, default=False)
args = parser.parse_args()

logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)
logging.debug('Arguments: %s', args)

overview = {}
def add_entry(wkn, name, value, data):
  logging.debug('Processing %s %s %d', wkn, name, value)
  logging.debug('Data is %s', data)
  if data.get('skip', False) == True:
    logging.debug('Skipping')
    return

  if 'split' in data:
    s = sum([x['ratio'] for x in data['split'].values()])
    logging.debug('Split calc, sum=%d', s)
    for (split_key, split_def) in data['split'].items():
      logging.debug('Split part %s %s', split_key, split_def)
      add_entry(
        '%s_%s' % (wkn, split_key),
        '%s (%s)' % (name, split_key),
        value * split_def['ratio'] / float(s),
        data = split_def
      )
  else:
    data['value'] = value
    overview[wkn] = data

# Skip over the first empty row
for f in args.file:
  logging.debug('Working on new file')

  # Check for broken Comdirect file starting in a single ';'
  if f.readline() == ';\n':
    # The broken line was consumed, everything fine
    logging.debug('Skipping invalid line')
  else:
    # The line was not broken, restart the handling
    f.seek(0)

  reader = csv.DictReader(f, delimiter=';', quotechar='"', skipinitialspace=True)
  for row in reader:
    logging.debug('Read row %s', row)

    # Check for required fields, otherwise skip completely without message
    if row['WKN'] == '' or row['WKN'] is None or row['Wert in EUR'] == '--':
      continue

    # This is a valid entry but doesn't contain any notes, skip over it
    if row['Notizen'] == '':
      logging.warning('No Notes for %s %s', row['WKN'], row['Bezeichnung'])
      continue

    data_yaml = yaml.load(row['Notizen'])
    if type(data_yaml) is not dict:
      logging.warn('Notes for %s %s are not an object but %s', row['WKN'], row['Bezeichnung'], type(data_yaml))
      continue

    add_entry(row['WKN'], row['Bezeichnung'], float(row['Wert in EUR'].replace('.', '').replace(',', '.')), data_yaml)

logging.debug('Collected data %s', pprint.pformat(overview))

def print_group_by(group_field, **where):
  money_per_group = defaultdict(lambda: 0)
  items_per_group = defaultdict(lambda: 0)
  money_sum = 0
  for (wkn, v) in overview.items():
    if group_field not in v:
      logging.warn('%s is missing field %s', wkn, field)
      continue

    if all([v[ff] == fv for (ff, fv) in where.items()]):
      money_per_group[v[group_field]] += v['value']
      items_per_group[v[group_field]] += 1
      money_sum += v['value']

  logging.debug('Money Sum: %d', money_sum)
  logging.debug('Money Field Sums: %s', money_per_group)
  logging.debug('Item Count per Field: %s', items_per_group)

  if len(where) == 0:
    print('Group by "%s"' % (group_field))
  else:
    print('Group by "%s" where %s:' % (group_field, " and ".join(['%s=%s' % (ff, fv) for (ff, fv) in where.items()])))
  for (field, value_sum) in sorted(money_per_group.items(), key=lambda x: -x[1]):
    print('  - %s (%d): %5.1f%% (%.2f€)' % (field.ljust(20), items_per_group[field], value_sum / money_sum * 100.0, value_sum))
  print('')

print_group_by('Art')
print_group_by('Ort')
print_group_by('Investment Art')
for a in ['Aktien', 'Anleihen', 'Tagesgeld']:
  print_group_by('Investment Art', Art=a)
  print_group_by('Ort', Art=a)
